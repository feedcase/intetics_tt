from django.db import models


class Sorting(models.Model):
    BUBBLE = 'Bubble'
    INSERTION = 'Insertion'
    MERGE = 'Merge'
    HEAP = 'Heap'
    QUICK = 'Quick'
    SELECTION = 'Selection'
    SORT_CHOICES = (
        (BUBBLE, 'Bubble'),
        (INSERTION, 'Insertion'),
        (MERGE, 'Merge'),
        (HEAP, 'Heap'),
        (QUICK, 'Quick'),
        (SELECTION, 'Selection'),
    )

    file_name = models.CharField(max_length=200)
    file_content = models.FileField('File', upload_to='files')
    algorithm = models.CharField(
        max_length=15,
        choices=SORT_CHOICES,
        default=BUBBLE,
    )
    result = models.CharField(max_length=250)

    class Meta:
        ordering = ('file_name',)

    def __str__(self):
        return self.file_name

