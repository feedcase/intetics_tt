from django import forms
from .models import Sorting

import sorting.services as services


class SortForm(forms.ModelForm):
    class Meta:
        model = Sorting
        fields = ('file_content', 'algorithm')

    def save(self, commit=True):
        form_data = self.cleaned_data
        file = services.normalizer(form_data['file_content'])
        algorithm = form_data['algorithm']
        self.instance.file_content = form_data['file_content']
        self.instance.algorithm = form_data['algorithm']
        self.instance.file_name = form_data['file_content'].name
        if algorithm == 'Bubble':
            self.instance.result = services.BubbleSort.sort_base(file)
        elif algorithm == 'Insertion':
            self.instance.result = services.InsertionSort.sort_base(file)
        elif algorithm == 'Merge':
            self.instance.result = services.MergeSort.sort_base(file)
        elif algorithm == 'Heap':
            self.instance.result = services.HeapSort.sort_base(file)
        elif algorithm == 'Quick':
            self.instance.result = services.QuickSort.sort_base(file)
        elif algorithm == 'Selection':
            self.instance.result = services.SelectionSort.sort_base(file)
        return super(SortForm, self).save(commit)

