from abc import ABC, abstractmethod
import time


def normalizer(list_file):
    content = list_file.read()
    content = content.decode('utf-8')
    if ',' in content:
        content = [int(x.strip()) for x in content.split(',')]
    else:
        content = [int(x) for x in content.split()]
    return content


def time_counter_decorator(method):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        res = method(*args, **kwargs)
        print('Execution time: {0} seconds'.format(time.time() - start_time))
        return res
    return wrapper


def heapify(nums, heap_size, root_index):
    largest = root_index
    left_child = (2 * root_index) + 1
    right_child = (2 * root_index) + 2
    if left_child < heap_size and nums[left_child] > nums[largest]:
        largest = left_child
    if right_child < heap_size and nums[right_child] > nums[largest]:
        largest = right_child
    if largest != root_index:
        nums[root_index], nums[largest] = nums[largest], nums[root_index]
        heapify(nums, heap_size, largest)


def partition(nums, low, high):
    pivot = nums[(low + high) // 2]
    i = low - 1
    j = high + 1
    while True:
        i += 1
        while nums[i] < pivot:
            i += 1

        j -= 1
        while nums[j] > pivot:
            j -= 1

        if i >= j:
            return j
        nums[i], nums[j] = nums[j], nums[i]


class Sort(ABC):
    @abstractmethod
    def sort_base(self):
        pass


class BubbleSort(Sort):
    @time_counter_decorator
    def sort_base(self):
        swapper = True
        while swapper:
            swapper = False
            for i in range(len(self) - 1):
                if self[i] > self[i + 1]:
                    self[i], self[i + 1] = self[i + 1], self[i]
                    swapper = True
        return self


class InsertionSort(Sort):
    @time_counter_decorator
    def sort_base(self):
        for i in range(1, len(self)):
            item_to_insert = self[i]
            j = i - 1
            while j >= 0 and self[j] > item_to_insert:
                self[j + 1] = self[j]
                j -= 1
            self[j + 1] = item_to_insert
        return self


class MergeSort(Sort):
    @time_counter_decorator
    def sort_base(self):
        sorted_list = []
        if len(self) <= 1:
            return self
        mid = len(self) // 2
        left_list = MergeSort.sort_base(self[:mid])
        right_list = MergeSort.sort_base(self[mid:])
        left_list_index = right_list_index = 0
        left_list_length, right_list_length = len(left_list), len(right_list)
        for i in range(left_list_length + right_list_length):
            if left_list_index < left_list_length and right_list_index < right_list_length:
                if left_list[left_list_index] <= right_list[right_list_index]:
                    sorted_list.append(left_list[left_list_index])
                    left_list_index += 1
                else:
                    sorted_list.append(right_list[right_list_index])
                    right_list_index += 1
            elif left_list_index == left_list_length:
                sorted_list.append(right_list[right_list_index])
                right_list_index += 1
            elif right_list_index == right_list_length:
                sorted_list.append(left_list[left_list_index])
                left_list_index += 1

        return sorted_list


class HeapSort(Sort):
    @time_counter_decorator
    def sort_base(self):
        list_length = len(self)
        for i in range(list_length, -1, -1):
            heapify(self, list_length, i)
        for i in range(list_length - 1, 0, -1):
            self[i], self[0] = self[0], self[i]
            heapify(self, i, 0)
        return self


class QuickSort(Sort):
    @time_counter_decorator
    def sort_base(self):
        def quick_sort(items, l, h):
            if l < h:
                split_index = partition(items, l, h)
                quick_sort(items, l, split_index)
                quick_sort(items, split_index + 1, h)
        quick_sort(self, 0, len(self) - 1)
        return self


class SelectionSort(Sort):
    @time_counter_decorator
    def sort_base(self):
        for i in range(len(self)):
            lowest_value_index = i
            for j in range(i + 1, len(self)):
                if self[j] < self[lowest_value_index]:
                    lowest_value_index = j
            self[i], self[lowest_value_index] = self[lowest_value_index], self[i]
        return self

