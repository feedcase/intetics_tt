from django.urls import path
import sorting.views as views


urlpatterns = [
    path('', views.upload_file, name='home'),
    path('sorted/', views.sorted_view, name='sorted')
]

