from django.contrib import admin
from .models import Sorting


@admin.register(Sorting)
class SortingAdmin(admin.ModelAdmin):
    model = Sorting
    list_display = ('file_name', 'file_content', 'algorithm', 'result')
    list_filter = ('algorithm',)
    search_fields = ('file_name', 'file_content', 'algorithm', 'result')
    list_display_links = ('file_name',)


