from django.shortcuts import render, redirect
from .models import Sorting
from .forms import SortForm


def upload_file(request):
    if request.method == 'POST':
        form = SortForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('sorted')
    else:
        form = SortForm()
    return render(request, 'home.html', {'form': form})


def sorted_view(request):
    sorted_lists = Sorting.objects.all()
    return render(request, 'sorted.html', {'sorted': sorted_lists})

