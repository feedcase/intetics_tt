# Generated by Django 3.1.7 on 2021-03-12 18:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sorting', '0003_auto_20210312_0750'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sorting',
            name='algorithm',
            field=models.CharField(choices=[('Bubble', 'Bubble'), ('Insertion', 'Insertion'), ('Merge', 'Merge'), ('Heap', 'Heap'), ('Quick', 'Quick'), ('Selection', 'Selection')], default='Bubble', max_length=15),
        ),
    ]
